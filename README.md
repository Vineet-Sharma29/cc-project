## Implementing Service Mesh Using Istio

* This project implements service mesh architecture, consisting of polyglot microservices, managed by `Istio`.

* The service-mesh balances load of the available(3) review services, which can be observed on refereshing. 

* This project also tries to get hands on traffic shifting, fault injection and request routing.

## Description

* The application displays information about a book. The book info can be broken down into following four services:-
![Architecture Image](https://istio.io/latest/docs/examples/bookinfo/noistio.svg)
    * A `Python` service to handle request for a book
    * A `Java` service to handle reviews of a book. This service has 3 versions - one without rating, one with black star rating and with red star rating.
    * A `Ruby` service to handle detail about a book
    * A `Node-JS` service to handle rating of a book

* `Istio` handle above micro-services using following architecture:-
![Architecture Image](https://istio.io/latest/docs/ops/deployment/architecture/arch.svg)
    * It consists of two planes - data plane, which contains intelligent `Envoy` proxies and control plane, which contains Mixer and Citadel
    * Each service is attached with language agnostic sidecar proxy which control communication between nodes, load balancing and collect telemetry data
    * Control plane is responsible for service discovery, enforcing securing policies and service-to-service, end-to-end authentication.

* Following diagram illustrates the deployment model with `Istio`:-
![Architecture Image](https://istio.io/latest/docs/examples/bookinfo/withistio.svg)

* For hosting the microservice on `Azure`. `AKS` cluster has following configuration:-
    * Number of nodes - `2`
    * Node Pool - `1`
    * Kubernetes version - `1.15.11`
